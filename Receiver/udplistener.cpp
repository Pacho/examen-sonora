#include "udplistener.h"

UDPListener::UDPListener(QObject *parent) : QObject(parent)
{
    socket = new QUdpSocket(this);
}

UDPListener::~UDPListener()
{
    delete socket;
}

void UDPListener::readyRead()
{
    QByteArray buffer;
    buffer.resize(socket->pendingDatagramSize());

    QHostAddress sender;
    quint16 senderPort;
    socket->readDatagram(buffer.data(), buffer.size(),
                         &sender, &senderPort);

    qDebug() << "Message from: " << sender.toString();
    qDebug() << "Message: " << buffer.data() << "\n";
}

void UDPListener::init()
{
    if(!socket->bind(QHostAddress::LocalHost, 1234)) qDebug() << "Error";
    qDebug() << "Listening...";
    QObject::connect(socket, &QUdpSocket::readyRead, this, &UDPListener::readyRead);
}
