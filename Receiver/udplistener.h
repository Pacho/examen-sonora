#ifndef UDPLISTENER_H
#define UDPLISTENER_H

#include <QObject>
#include <QtNetwork/QUdpSocket>

class UDPListener : public QObject
{
    Q_OBJECT
public:
    explicit UDPListener(QObject *parent = nullptr);
    ~UDPListener();

signals:

public slots:
    void readyRead();
    void init();

private:
     QUdpSocket *socket;
};

#endif // UDPLISTENER_H
