#include <QCoreApplication>
#include "udplistener.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    UDPListener listener;
    listener.init();

    return a.exec();
}
