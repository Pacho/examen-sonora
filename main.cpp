#include <QCoreApplication>
#include <QCommandLineParser>
#include <QObject>
#include <QDebug>
#include <QJsonObject>
#include <udpclient.h>
#include "mainclass.h"


int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationName("SonoraExam");
    QCoreApplication::setApplicationVersion("1.0");

    Mainclass app(argc, argv);
    UDPClient *client = new UDPClient();

    QJsonObject info = app.parseCmdLineOptions();
    client->sendInfo(info);

    return app.exec();
}
