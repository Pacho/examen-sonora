#ifndef UDPCLIENT_H
#define UDPCLIENT_H

#include <QObject>
#include <QtNetwork/QUdpSocket>
#include <QJsonObject>

class UDPClient : public QObject
{
    Q_OBJECT
public:
    explicit UDPClient(QObject *parent = nullptr);

signals:

public slots:
    void sendInfo(QJsonObject info);

private:
    QUdpSocket *socket;
};

#endif // UDPCLIENT_H
