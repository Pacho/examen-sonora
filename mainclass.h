#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QJsonDocument>
#include <QJsonObject>

class Mainclass : public QCoreApplication
{
public:
    explicit Mainclass(int argc, char **argv, int flags = QCoreApplication::ApplicationFlags);
    QJsonObject parseCmdLineOptions();

signals:
    void onInfo(QJsonObject);

public slots:

private:
     QJsonObject m_config;
};

#endif // MAINCLASS_H
