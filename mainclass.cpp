#include "mainclass.h"
#include <QJsonObject>
#include <QDebug>

Mainclass::Mainclass(int argc, char **argv, int flags) : QCoreApplication(argc, argv, flags)
{

}

QJsonObject Mainclass::parseCmdLineOptions()
{
    QCommandLineParser parser;
    parser.setApplicationDescription("Interview Test");
    parser.addHelpOption();


    parser.addPositionalArgument(".txt filename", QCoreApplication::translate("main", "Please insert name of your .txt file"));
    parser.addPositionalArgument("Path", QCoreApplication::translate("main", "Please insert the path where your .txt file will be saved"));
    parser.addPositionalArgument("Text", QCoreApplication::translate("main", "Please insert text you wish to be written into .txt file"));
    parser.process(*this);


    const QStringList posArgs = parser.positionalArguments();
    if(posArgs.count() != 3) {
        parser.showHelp();
    }

    m_config.insert("filename", posArgs[0]);
    m_config.insert("path", posArgs[1]);
    m_config.insert("text", posArgs[2]);

    return m_config;

}
