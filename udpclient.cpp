#include "udpclient.h"
#include <QFile>

UDPClient::UDPClient(QObject *parent) : QObject(parent)
{
    socket = new QUdpSocket(this);
}

void UDPClient::sendInfo(QJsonObject info)
{
    QString file_name = info.value("filename").toString();
    QString path = info.value("path").toString();
    QString text = info.value("text").toString();

    QFile file(path + "/" + file_name + ".txt");
    if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
        return;

    QTextStream out(&file);
    QString content;
    out << text;
    content = out.readAll();

    QByteArray Data;
    Data.append(content);
    socket->writeDatagram(Data, QHostAddress::LocalHost, 1234);
}
